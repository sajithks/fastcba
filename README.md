# Faster CBA algorithm for Segmentation of E. coli Bacterial cells #
Live cell experiments are important to understand the complex biological function of living organisms. Many live cell experiments require monitoring of cells under different conditions over several generations. Isogenic cells display cell-to-cell variability even when grown under similar conditions. To study the origin and consequences of such variation it is necessary to monitor many individual cells for extended periods of time to reach statistically testable conclusions. Time-lapse experiments usually generate large quantities of data, which become extremely difficult for human observers to evaluate in an unbiased way. Thus, automated systems are necessary to analyze such datasets in order to reach robust and reproducible results.

We developed algorithm for segmentation of E. coli bacterial cells in paper [1]. The aim of this project is to speed up the algorithm for segmentation of E. coli bacteria cells. The original algorithm was written as a multithreaded program in python. We showed in the paper that our method is faster than the state-of-the-art method to segment similar cells. We had the idea that the python code can be ported to C++ but the task of writing a full-fledged C++ program was not a trivial task. Now through this course, we keep the structure of original python code as such and a part of the program that is compute intensive is ported to C++.

##Methodology##
The project consists of two parts – first part written in python and second part written in C++. Techniques that were used in this project include 

1. Boost python module, for interaction between C++ and python parts of code.
2. For loops using auto key word, which is part of C++11.
3. Multi threaded implementation of a critical part of the algorithm using packaged task.

##Implementation details##
###Python code ###
At first, the input image, Fig. 1, is read in python using OpenCV library [3]. Then the input image is preprocessed to enhance the contrast so that the separation s between touching cells are more visible, Fig 2. Finding the lowest Eigen value of Hessian matrix of the image does the contrast enhancement. The next step in the algorithm is to repeatedly threshold the contrast-enhanced image to extract individual cells. When we threshold this image we get lot of separate objects and all of them are uniquely labeled. The next step is to identify cells from these objects. To identify cells we use a weight function, which is a combination of ellipse parameter and convexity of individual objects. At this stage we feed the algorithm with a few parameters that include the major and minor axes length of expected cells present in the image. This weight finding is the most time consuming part of the entire algorithm, as we have to find the weights of thousands of objects. This weight calculation is what we write in C++ for increased speed.

###C++ code###

As mentioned before the compute intensive weight calculation is ported to C++. The interaction between python and C++ is through boost-python interface. We have adopted the code for numpy array-OpenCV mat conversion form [2] and modified for our needs. The input to C++ requires the image to be 2D but we have a 3D image stack, after thresholding and labeling. So each image in the 3D stack is flattened and a 2D image is created for transferring to C++. Once we get the python object in C++ as OpenCV mat object we have the library support from OpenCV library also.

To the test the performance of the algorithm we wrote the program as a single threaded version and also a multi-threaded version. In the program, we replaced all the traditional for loop with that with auto keyword. We did multi-threaded implementation using standard packaged task. We used cmake for building the entire program.

##Installation Details##
To run the program we need to meet the following dependencies in Ubuntu 14.04. 

* Python
* Opencv for python
* Boost, boost-python, boost-system
* Scipy, numpy

To build the system the following commands are used.

* Edit Makefile to add current folder.

```$cmake CMakeLists.txt```

```$make```

To run the program run

```$python fcba.py
```
##References##
1. Sadanandan, S.K., O. Baltekin, K.E.G. Magnusson, A. Boucharin, P. Ranefall, J. Jalden, J. Elf, and C. Wahlby. “Segmentation and Track-Analysis in Time-Lapse Imaging of Bacteria.” IEEE Journal of Selected Topics in Signal Processing 10, no. 1 (February 2016): 174–84. doi:10.1109/JSTSP.2015.2491304.
2. https://github.com/spillai/numpy-opencv-converter
3. G. Bradski, A. Kaehler, “ Learning OpenCV: Computer Vision with the OpenCV Library” O’Reilly Media Inc, 2008.